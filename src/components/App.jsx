import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Note from './note'
import notesData from '../notes'


function App() {
  return (
    <div>
      <Header />
      <Footer />
     {notesData.map( (note) => <div><Note title= {note.title} content= {note.content}/></div>)}
    </div>
  );
}

export default App;